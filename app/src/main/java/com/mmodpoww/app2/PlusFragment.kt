package com.mmodpoww.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.navigation.findNavController
import com.mmodpoww.app2.databinding.ActivityMainBinding
import com.mmodpoww.app2.databinding.FragmentHomeBinding
import com.mmodpoww.app2.databinding.FragmentPlusBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusFragment : Fragment() {
    private var _binding: FragmentPlusBinding? = null
    private val binding get() = _binding
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPlusBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.equalplus?.setOnClickListener {
            val num1: Int = view.findViewById<EditText>(R.id.num1).text.toString().toInt()
            val num2: Int = view.findViewById<EditText>(R.id.num2).text.toString().toInt()
            val answer = num1 + num2
            val action = PlusFragmentDirections.actionPlusFragment2ToAnswerFragment(answer)
            view.findNavController().navigate(action)
        }
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PlusFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}